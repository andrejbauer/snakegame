
from tkinter import *
from random import *
import random

visina = 50
sirina = 50
dolzina = 10
dx = [0,1,0,-1]
dy = [-1,0,1,0]

obratna_smer = [2, 3, 0,1]

class Snake():
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina, barva, barva_glave):
        self.canvas = canvas # Canvas, na katerem je kača narisana
        self.telo = []
        for i in range(dolzina):
            x = glava_x + dx[obratna[smer]] * i
            y = glava_y + dy[obratna[smer]] * i
            barva_ovala = (barva_glave if i == 0 else barva)
            id = canvas.create_oval(x*10,y*10,(x+1)*10,(y+1)*10,width=5,outline=barva_ovala)
            self.telo.append((x,y,id))
        self.smer = smer
        self.barva = barva
        self.barva_glave = barva_glave

class HumanSnake(Snake):
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina):
        super().__init__(canvas, glava_x, glava_y, smer, dolzina, "green", "deep pink")

class ComputerSnake(Snake):
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina):
        super().__init__(canvas, glava_x, glava_y, smer, dolzina, "turquoise", "yellow")


class SnakeGame():
    def __init__(self,master):
        #Meni (v nastajanju)
        menu = Menu(master)
        master.config(menu=menu) #Dodamo meni.

        game_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Game", menu=game_menu)
        game_menu.add_command(label="New Game        (R)", command=self.50)
        game_menu.add_command(label="Quit Game      (Esc)", command=self.quit)
        game_menu.add_separator()
        game_menu.add_command(label="About", command=self.about)
        game_menu.add_command(label="Help", command=self.zacasna)

        player_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Player", menu=player_menu)
        player_menu.add_command(label="Me", command=self.me)
        player_menu.add_command(label="Me vs. Computer", command=self.me_vs_computer)

        difficulty_menu = Menu(menu, tearoff=0)  #???izberi glede na player_menu
        menu.add_cascade(label="Difficulty", menu=difficulty_menu)
        difficulty_menu.add_command(label="Easy", command=self.zacasna)
        difficulty_menu.add_command(label="Medium", command=self.zacasna)
        difficulty_menu.add_command(label="Hard", command=self.zacasna)


        #Ustvarimo platno
        self.platno = Canvas(master, width = sirina * 10, height = visina * 10, bg = "black") #ustvarimo platno
        self.platno.pack()

        #Upravljanje tipk
        self.platno.bind_all("<Left>",self.levo) #pritisnemo levo za spremembo smeri kace v levo (podobno za desno,gor in dol)
        self.platno.bind_all("<Right>",self.desno)
        self.platno.bind_all("<Up>",self.gor)
        self.platno.bind_all("<Down>",self.dol)
        self.platno.bind_all("<Escape>",self.zapri_okno) #pritisnemo Escape za konec (zapre okno)
        self.platno.bind_all("<s>",self.zacni_igro)  #pritisnemo s za zacetek
        self.platno.bind_all("<r>",self.nova_igra)

        self.alone = True

        self.ponastavi()

        #######Definiramo osnovne spremenljivke za igralca#######

    def new_game(self):
        """Nastavi vse za začetek igre."""
        # Pobrišemo platno
        self.platno.delete(ALL)
        # Nastavimo polje
        self.polje = [ [ 0 for i in range(0,50) ] for j in range(0,50) ]
        # Naredimo kace
        self.kace = []
        # naredimo clovekovo kaco
        x = randint(dolzina+1,sirina-dolzina-1)  #x koordinata glave
        y = randint(dolzina+1,visina-dolzina-1)  #y koordinata glave
        smer = randint(0,3)
        self.kace.append(HumanSnake(self.platno, x, y, smer, dolzina))
        # ce je treba, naredimo se racunalnikovo kaco
        if not self.alone:
            smer_rac = randint(0,3)
            zac_razdalja_kac = 0
            while zac_razdalja_kac <= 20: #Dokler je razdalja med glavama kac <= 20, izberi nov (x,y).
                x_rac = randint(dolzina,sirina-dolzina)
                y_rac = randint(dolzina,visina-dolzina)
                zac_razdalja_kac = abs(self.x - x_rac) + abs(self.y - y_rac)
            self.kace.append(ComputerSnake(self.platno, x_rac, y_rac, smer_rac, dolzina))
        #
        for kaca in self.kace:
            kaca.narisi(self.canvas)

        # prekopiraj iz ponastavi se vse v zvezi s hrano in napisi
        pass

    def ponastavi(self):

        #Izberemo random zacetne koordinate glave + smer kace
        self.x = randint(dolzina+1,sirina-dolzina-1)  #x koordinata glave
        self.y = randint(dolzina+1,visina-dolzina-1)  #y koordinata glave
        self.smer = randint(0,3)
        self.obratna_smer = (self.smer + 2)%4

        #MATRIKA VREDNOSTI IN SEZNAM KOORDINAT
        self.polje = [ [ 0 for i in range(0,50) ] for j in range(0,50) ]  #ustvari matriko nicel velikosti 50 x 50
        self.kaca = [] #seznam kaca: tu notri bomo shranjevali trojice (x koordinata kace, y koordinata kace,id narisanega polja, npr. [(32,24,5)])

        for i in range(1,dolzina):   #dodamo enke v matriko, kjer je začetna kaca (brez glave)
            self.x_telo = self.x+i*dx[self.obratna_smer]
            self.y_telo = self.y+i*dy[self.obratna_smer]
            self.polje[self.y_telo][self.x_telo] = 1
            id_zacetna_kaca = self.platno.create_oval(self.x_telo*10,self.y_telo*10,(self.x_telo+1)*10,(self.y_telo+1)*10,width=5,outline="green") #narisemo telo

            self.kaca.append((self.x_telo,self.y_telo,id_zacetna_kaca)) #seznam koordinat,kjer je kaca (brez glave)

        self.polje[self.y][self.x] = 2 #dodamo 2,kjer je glava kace
        id_zacetna_glava = self.platno.create_oval(self.x*10,self.y*10,(self.x+1)*10,(self.y+1)*10,width=5,outline="deep pink") #narisemo glavo
        self.kaca.insert(0,(self.x,self.y,id_zacetna_glava))

        ####OSTALO####
        self.id_hrana = 0 #tu bomo shranjevali "id-je" narisane hrane
        self.x_hrana = 0 #x koordinata hrane
        self.y_hrana = 0 #y koordinata hrane
        self.polje_hrana = False #to potrebujemo, da se hrana ne pojavi dokler ne zgine prejsnja
        self.konec = False #to potrebujemo, da vemo kdaj koncati igro
        self.hitrost = 100 #zacetna hitrost
        self.score = 0 #zacetno stevilo pobrane hrane
        self.zmaga_igralec = None

        self.text_score = self.platno.create_text(460, 20,fill = "green",text = "SCORE:" + str(self.score)) #dodamo text (score:0)
        self.text_id = self.platno.create_text(90, 40,fill = "white",text ="  PRESS S TO START")

        if self.alone == False:
            self.ponastavi_rac()
            self.text_score_rac = self.platno.create_text(430, 40,fill = "turquoise",text = "SCORE COMPUTER:" + str(self.score_rac)) #dodamo text (score:0)


        #######Definiramo osnovne spremenljivke za racunalnik#######
    def ponastavi_rac(self):
        self.smer_rac = randint(0,3)
        self.obratna_smer_rac = (self.smer_rac + 2)%4
        self.x_rac = randint(dolzina+1,sirina-dolzina-1)  #x koordinata glave
        self.y_rac = randint(dolzina+1,visina-dolzina-1)  #y koordinata glave

        self.zac_razdalja_kac = abs(self.x - self.x_rac) + abs(self.y - self.y_rac)
        while self.zac_razdalja_kac <= 20: #Dokler je razdalja med glavama kac <= 20, izberi nov (x,y).
            self.x_rac = randint(dolzina,sirina-dolzina)
            self.y_rac = randint(dolzina,visina-dolzina)
            self.zac_razdalja_kac = abs(self.x - self.x_rac) + abs(self.y - self.y_rac)



        #MATRIKA VREDNOSTI IN SEZNAM KOORDINAT
        self.kaca_rac = [] #seznam koordinat kace za racunalnik

        for i in range(1,dolzina):   #dodamo enke v matriko, kjer je začetna kaca (brez glave)
            self.x_telo_rac = self.x_rac+i*dx[self.obratna_smer_rac]
            self.y_telo_rac = self.y_rac+i*dy[self.obratna_smer_rac]
            self.polje[self.y_telo_rac][self.x_telo_rac] = 3
            self.id_zacetna_kaca_rac = self.platno.create_oval(self.x_telo_rac*10,self.y_telo_rac*10,(self.x_telo_rac+1)*10,(self.y_telo_rac+1)*10,width=5,outline="turquoise") #narisemo glavo
            self.kaca_rac.append((self.x_telo_rac,self.y_telo_rac,self.id_zacetna_kaca_rac)) #seznam koordinat,kjer je kaca (brez glave)

        self.polje[self.y_rac][self.x_rac] = 4 #dodamo 4,kjer je glava kace
        id_zacetna_glava_rac = self.platno.create_oval(self.x_rac*10,self.y_rac*10,(self.x_rac+1)*10,(self.y_rac+1)*10,width=5,outline="yellow") #narisemo glavo
        self.kaca_rac.insert(0,(self.x_rac,self.y_rac,id_zacetna_glava_rac))

        self.score_rac = 0 #zacetno stevilo pobrane hrane

    ##################FUNKCIJA, KI PREMAKNE KACO-IGRALEC####################
    def premakni(self):
        self.platno.delete(self.text_id) #izbrisemo text (Start)

        ###Igralec (koordinate)
        self.rep = self.kaca[(len(self.kaca))-1] #iz seznama kaca vzamemo koordinate repa
        self.x_rep = self.rep[0] #x koordinata repa
        self.y_rep = self.rep[1]
        self.glava = self.kaca[0] #iz seznama kaca vzamemo koordinate glave
        self.x_glava = self.glava[0] #x koordinata glave
        self.y_glava = self.glava[1] #y koordinata glave
        self.nov_x_glava = self.x_glava + dx[self.smer] #spremenimo x v tisto smer, v katero se premika kaca
        self.nov_y_glava = self.y_glava + dy[self.smer] #isto za y



        if self.konec == False: #ce se ni konec igre, preverimo, ce se zaleti, postavimo hrano itd.
            self.preverjanje_polj()

        if self.konec == False:  #ce se ni konec, narisemo premik
            self.narisi_premik()


            if self.alone == False:  #if not self.alone
                self.algoritem_rac()  #s tem dobimo nov x,y glave (kamor se bo premaknila): self.nov_x_rac in self.nov_y_rac
                self.preverjanje_polj_rac()
                self.narisi_premik_rac()

            self.klici_premakni = self.platno.after(self.hitrost,self.premakni)

        else:  #Ce je konec (npr. ce smo se zaleteli) se izpise napis Game over ipd.
            if self.zmaga_igralec == True:
                self.platno.create_text(200, 200,fill = "white",text = "You win!")
                self.text2_id = self.platno.create_text(90, 40,fill = "white",text ="Press R to Restart \n\n Press Esc To quit")
            elif self.zmaga_igralec == False:
                self.platno.create_text(200, 200,fill = "white",text = "You lose! Press R and Try again!")

    ##################FUNKCIJA, KI NARISE PREMIK (+SPREMENI VREDNOSTI V MATRIKI IN SEZNAM KOORDINAT-IGRALEC####################
    def narisi_premik(self):
            #RISANJE:(1) zbrisemo rep (2) zbrisemo staro glavo, (3) narisemo staro glavo z drugo barvo (telo). (4) narisemo novo glavo

            #(1)
            self.trojica_rep = self.kaca[len(self.kaca)-1]
            self.platno.delete(self.trojica_rep[2])

            #(2)
            self.trojica_stara_glava = self.kaca[0]
            self.platno.delete(self.trojica_stara_glava[2])

            #(3)
            self.id_stara_glava = self.platno.create_oval(self.x_glava*10,self.y_glava*10,(self.x_glava+1)*10,(self.y_glava+1)*10,width=5,outline="green")

            self.kaca.pop(0)  #v trojici stare glave moramo zamenjati ID: izbrisemo celo trojico stare glave
            self.kaca.insert(0,(self.x_glava,self.y_glava,self.id_stara_glava)) #in dodamo novo

            #SPREMEMBE V SEZNAMU KACA
            self.kaca.insert(0,(self.nov_x_glava,self.nov_y_glava)) #na zacetek seznama dodamo koordinate premaknjene glave
            self.kaca.pop(len(self.kaca)-1) #izbrisemo rep

            #SPREMEMBE V MATRIKI
            self.polje[self.y_glava][self.x_glava] = 1 #v matriko vrednosti dodamo 1, kjer je bla prej glava
            self.polje[self.y_rep][self.x_rep] = 0 #dodamo 0, kjer je bil prej rep
            self.polje[self.nov_y_glava][self.nov_x_glava] = 2 #dodamo 2, kjer je nova glava

            #(4)
            self.id_kaca_glava = self.platno.create_oval(self.nov_x_glava*10,self.nov_y_glava*10,(self.nov_x_glava+1)*10,(self.nov_y_glava+1)*10,width=5,outline="deep pink") #narisemo novo glavo in shranimo id nove glave
            self.kaca[0] += (self.id_kaca_glava,) #id nove glave dodamo v zacetni element kace, na zadnje mesto

            print(str(self.nov_x_glava) + " nov_x_glava")
            print(str(self.nov_y_glava) + " nov_y_glava")

    ##################FUNKCIJA, KI NARISE PREMIK (+SPREMENI VREDNOSTI V MATRIKI IN SEZNAM KOORDINAT-RACUNALNIK####################
    def narisi_premik_rac(self):
            #(1)
            self.trojica_rep_rac = self.kaca_rac[len(self.kaca_rac)-1]
            self.platno.delete(self.trojica_rep_rac[2])

            #(2)
            self.trojica_stara_glava_rac = self.kaca_rac[0]
            self.platno.delete(self.trojica_stara_glava_rac[2])

            #(3)
            self.id_stara_glava_rac = self.platno.create_oval(self.x_glava_rac*10,self.y_glava_rac*10,(self.x_glava_rac+1)*10,(self.y_glava_rac+1)*10,width=5,outline="turquoise")

            #v trojici stare glave moramo zamenjati ID
            self.kaca_rac.pop(0) #zbrisemo celo trojico stare glave
            self.kaca_rac.insert(0,(self.x_glava_rac,self.y_glava_rac,self.id_stara_glava_rac))

            #SPREMEMBE V SEZNAMU KACA
            self.kaca_rac.insert(0,(self.nov_x_rac,self.nov_y_rac)) #na zacetek seznama dodamo koordinate premaknjene glave
            self.kaca_rac.pop(len(self.kaca_rac)-1) #izbrisemo rep

            #SPREMEMBE V MATRIKI
            self.polje[self.y_glava_rac][self.x_glava_rac] = 3 #v matriko vrednosti dodamo 3, kjer je bla prej glava
            self.polje[self.y_rep_rac][self.x_rep_rac] = 0 #dodamo 0, kjer je bil prej rep
            self.polje[self.nov_y_rac][self.nov_x_rac] = 4 #dodamo 4, kjer je nova glava

            #(4)
            self.id_kaca_glava_rac = self.platno.create_oval(self.nov_x_rac*10,self.nov_y_rac*10,(self.nov_x_rac+1)*10,(self.nov_y_rac+1)*10,width=5,outline="yellow") #narisemo novo glavo in shranimo id nove glave
            self.kaca_rac[0] += (self.id_kaca_glava_rac,) #id nove glave dodamo v zacetni element kace, na zadnje mesto

            print(str(self.nov_x_rac) + " nov_x_rac")
            print(str(self.nov_y_rac) + " nov_y_rac")

    ##################FUNKCIJA, KI PREMAKNE KACO-RACUNALNIK####################

    def algoritem_rac(self):

        ###Racunalnik (koordinate)
        self.rep_rac = self.kaca_rac[(len(self.kaca_rac))-1] #iz seznama kaca vzamemo koordinate repa
        self.x_rep_rac = self.rep_rac[0] #x koordinata repa
        self.y_rep_rac = self.rep_rac[1]
        self.glava_rac = self.kaca_rac[0]
        self.x_glava_rac = self.glava_rac[0]
        self.y_glava_rac = self.glava_rac[1]
        self.mozne_smeri = [self.smer_rac,(self.smer_rac + 1)%4,(self.smer_rac - 1)%4] #seznam moznih smeri
        self.polje_min_razdalja = 100000  #Tu shranjujemo minimalno razdaljo.
        self.stevec_nepraznih_polj = 0 #Steje neprazna polja.

        self.trojice = []
        for i in range(len(self.mozne_smeri)):
            self.nov_x_rac = self.x_glava_rac + dx[self.mozne_smeri[i]] #Izracunamo nov x in nov y za vse mozne smeri.
            self.nov_y_rac = self.y_glava_rac + dy[self.mozne_smeri[i]]
            if self.nov_x_rac == 50 or self.nov_x_rac == -1 or self.nov_y_rac == 50 or self.nov_y_rac == -1:
                self.stevec_nepraznih_polj += 1
                continue

            if self.polje[self.nov_y_rac][self.nov_x_rac] == 5:  #Ce je na polju hrana, funkcija vrne nov (x,y) polja, kjer lezi hrana.
                self.smer_rac = self.mozne_smeri[i]
                return(self.nov_x_rac,self.nov_y_rac,self.smer_rac)

            elif self.polje[self.nov_y_rac][self.nov_x_rac] == 0:
                self.razdalja_hrana = abs(self.nov_x_rac - self.x_hrana) + abs(self.nov_y_rac - self.y_hrana)
                self.smer_rac = self.mozne_smeri[i]
                self.trojice.append((self.nov_x_rac,self.nov_y_rac,self.razdalja_hrana,self.smer_rac))
            else:
                self.stevec_nepraznih_polj += 1

        if self.stevec_nepraznih_polj == 3:  #Ce ni nobenega praznega polja, koncaj igro.
            self.konec = True   #konec igre-racunalnik se zaleti (zgubi?)
            self.zmaga_igralec = True

        else:
            random.shuffle(self.trojice) #Premesamo seznam, da je koordinata (x,y) z najmanjso razdaljo izbrana nakljucno.

            self.trojica_min = min(self.trojice, key = lambda t: t[2])

            self.nov_x_rac = self.trojica_min[0]
            self.nov_y_rac = self.trojica_min[1]
            self.smer_rac = self.trojica_min[3]
            return(self.nov_x_rac,self.nov_y_rac,self.smer_rac)


    ###################FUNKCIJA, KI PREVERJA POLJA##################
    def preverjanje_polj(self):
        #Ce se ni hrane, jo postavi.
        if not self.polje_hrana:
            self.hrana()

        #Ce je glava igralca na hrani, jo poje.
        if self.nov_x_glava == self.x_hrana and self.nov_y_glava == self.y_hrana:
            self.poje_hrano()


        #Ce se zaleti sama v sebe, je konec igre.
        for i in range(1,len(self.kaca)):
            self.del_telesa = self.kaca[i] #i-ti par (x,y) telesa
            self.x_del_telesa = self.del_telesa[0] #i-ta x koordinata telesa
            self.y_del_telesa = self.del_telesa[1] #i-ta y koordinata telesa
            if self.nov_x_glava == self.x_del_telesa and self.nov_y_glava == self.y_del_telesa:
                self.konec = True
                self.zmaga_igralec = False

        #Ce se igralec zaleti v rob, je konec igre
        if self.nov_x_glava == -1 or self.nov_x_glava == 50 or self.nov_y_glava == -1 or self.nov_y_glava == 50:
            self.konec = True
            self.zmaga_igralec = False



    def preverjanje_polj_rac(self):
        #Ce je glava racunalnika na hrani, jo poje.
        if self.nov_x_rac == self.x_hrana and self.nov_y_rac == self.y_hrana:
            self.poje_hrano_rac()

        #Ce se racunalnik zaleti v rob, je konec igre
        if self.nov_x_rac == -1 or self.nov_x_rac == 50 or self.nov_y_rac == -1 or self.nov_y_rac == 50:
            self.konec = True
            self.zmaga_igralec = True

        #Ce se zaleti v drugo kaco, je konec igre.
        for i in range(0,len(self.kaca_rac)):
            self.del_telesa_rac = self.kaca_rac[i]
            self.x_del_telesa_rac = self.del_telesa_rac[0]
            self.y_del_telesa_rac = self.del_telesa_rac[1]
            if self.nov_x_glava == self.x_del_telesa_rac and self.nov_y_glava == self.y_del_telesa_rac:
                self.konec = True
                self.zmaga_igralec = False



    ###################FUNKCIJA, KI POSTAVI HRANO##################
    def hrana(self):
        self.x_hrana = randint(1,sirina-2) #Postavimo hrano na "random" mesto (ne more biti na robu).
        self.y_hrana = randint(1,visina-2)

        if self.polje[self.y_hrana][self.x_hrana] == 0: #Hrana se ustvari samo, Ce je polje prazno.
            self.polje[self.y_hrana][self.x_hrana] = 5  #Vrednost polja v matriki, kjer je hrana je 5.
            self.id_hrana = self.platno.create_rectangle(self.x_hrana*10,self.y_hrana*10,(self.x_hrana+1)*10,(self.y_hrana+1)*10,width=1,outline="pink")
            self.polje_hrana = True


    ###################FUNKCIJA ZA DALJSANJE KACE - IGRALEC, KO POJE HRANO##################
    def poje_hrano(self):
        self.id_dodan_rep = self.platno.create_oval(self.x_rep*10, self.y_rep*10,(self.x_rep+1)*10,(self.y_rep+1)*10,width=5,outline="green") #narišemo podaljšani del kače
        self.polje[self.y_rep][self.x_rep] = 1
        self.kaca.append((self.x_rep,self.y_rep,self.id_dodan_rep))
        self.platno.delete(self.id_hrana)
        self.polje_hrana = False

        self.platno.delete(self.text_score) #zbrisemo prejsnji "score"
        self.score += 1 #stevec za "score"
        self.text_score = self.platno.create_text(460, 20,fill = "green",text = "SCORE:" + str(self.score)) #ponovno izpisemo "score"

        if self.alone == True:
            if len(self.kaca) == 200:
                self.konec = True
                self.zmaga_igralec = True

            if not self.hitrost == 10: #Ko kaca pobere hrano se hitrost poveca za 1ms.
                self.hitrost = self.hitrost - 1

    ###################FUNKCIJA ZA DALJSANJE KACE - RACUNALNIK, KO POJE HRANO##################
    def poje_hrano_rac(self):
        self.id_dodan_rep_rac = self.platno.create_oval(self.x_rep_rac*10, self.y_rep_rac*10,(self.x_rep_rac+1)*10,(self.y_rep_rac+1)*10,width=5,outline="turquoise") #narišemo podaljšani del kače
        self.polje[self.y_rep_rac][self.x_rep_rac] = 3
        self.kaca_rac.append((self.x_rep_rac,self.y_rep_rac,self.id_dodan_rep_rac))
        self.platno.delete(self.id_hrana)
        self.polje_hrana = False

        self.platno.delete(self.text_score_rac) #zbrisemo prejsnji "score"
        self.score_rac += 1 #stevec za "score"
        self.text_score_rac = self.platno.create_text(430, 40,fill = "turquoise",text = "SCORE COMPUTER:" + str(self.score_rac)) #ponovno izpisemo "score"



    ###################FUNKCIJE ZA KLIC TIPK##################
    def desno(self,event):   #Ce pritisnemo desno, se smer kace obrne v desno.
        if self.smer == 0  or self.smer == 2:
            self.smer = 1

    def levo(self,event):
        if self.smer == 0  or self.smer == 2:
            self.smer = 3

    def dol(self,event):
        if self.smer == 1  or self.smer == 3:
            self.smer = 2

    def gor(self,event):
        if self.smer == 1  or self.smer == 3:
            self.smer = 0


    def zapri_okno(self, event): #Funkcija, ki jo klicemo, ko pritisnemo tipko Escape.
        self.platno.master.destroy()

    def zacni_igro(self,event): #Funkcija, ki jo klicemo, ko pritisnemo tipko s.
        self.premakni()

    ###################FUNKCIJA ZA RESTART##################
    def nova_igra(self,event):
        self.platno.delete(ALL)
        self.ponastavi()

    ###funkciji za Menu (funkcija s parametri (self,event) ne deluje)
    def quit(self):
        self.platno.master.destroy()

    ###Pomozne funkcije###
    def zacasna(self):
        pass

    def about(self): #(POPRAVI!!!!)
        self.platno_about = Canvas(root, width = 100, height = 100, bg = "white") #ustvarimo platno
        self.platno_about.pack()

    def me(self):
        self.alone = True
        self.new_game()

    def me_vs_computer(self):
        self.alone = False
        self.new_game()


root = Tk()   #nardi osnovno, prazno,okno, še ne pokaže - naložiš gumbe,platno,....
root.title("Snake")  #ime
aplikacija = SnakeGame(root)  #to naredi objekt pod imenom aplikacija
root.mainloop()
