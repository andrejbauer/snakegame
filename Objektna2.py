from tkinter import *
from random import *
import random

visina = 50
sirina = 50
dolzina = 10
dx = [0,1,0,-1]
dy = [-1,0,1,0]

obratna = [2, 3, 0,1]

##############################################################################################################################
class Snake():
    def __init__(self, igra, glava_x, glava_y, smer, dolzina, barva, barva_glave):
        self.igra = igra # Objekt, ki predstavlja igro (razred SnakeGame)
        self.telo = []
        for i in range(dolzina):
            x = glava_x + dx[obratna[smer]] * i
            y = glava_y + dy[obratna[smer]] * i
            barva_ovala = (barva_glave if i == 0 else barva)
            id = self.igra.platno.create_oval(x*10,y*10,(x+1)*10,(y+1)*10,width=5,outline=barva_ovala)
            self.telo.append((x,y,id))
            self.igra.polje[y][x] = 1
        self.smer = smer
        self.barva = barva
        self.barva_glave = barva_glave
        self.tocke = 0

    def premakni(self):
        # izracunamo nove koordinate glave
        (x_glava, y_glava, id_glava) = self.telo[0]
        x_nova_glava = x_glava + dx[self.smer] #spremenimo x v tisto smer, v katero se premika kaca
        y_nova_glava = y_glava + dy[self.smer] #isto za y
        # ugotovimo, ali se bomo zaleteli
        if ((x_nova_glava == -1 or x_nova_glava == sirina or y_nova_glava == -1 or y_nova_glava == visina) or
           self.igra.polje[y_nova_glava][x_nova_glava] == 1):
            # smo se zaleteli
            return True
        else:
            # Ali bomo pojedli hrano?
            if self.igra.hrana is not None:
              (x_hrana, y_hrana, id_hrana) = self.igra.hrana
              if x_nova_glava == x_hrana and y_nova_glava == y_hrana:
                # Pojej hrano (in ne postavi nove, to dela nekdo drug)
                self.igra.odstrani_hrano()
                self.tocke += 1
            # se premaknemo
            # (1) zbrisemo rep
            (x_rep, y_rep, id_rep) = self.telo.pop()
            self.igra.polje[y_rep][x_rep] = 0
            self.igra.platno.delete(id_rep)
            # (2) stari glavi zamenjamo barvo
            self.igra.platno.itemconfig(id_glava, outline=self.barva)
            # (3) narisemo novo glavo
            id_nova_glava = self.igra.platno.create_oval(
              x_nova_glava*10, y_nova_glava*10, (x_nova_glava+1)*10, (y_nova_glava+1)*10,
              width=5, outline=self.barva_glave)
            self.telo.insert(0, (x_nova_glava, y_nova_glava, id_nova_glava))
            self.igra.polje[y_nova_glava][x_nova_glava] = 1
            return False

    def obrni(self):
      """AI, ki nastavi smer kace."""
      # Ta funkcija samo nastavi self.smer (ali pusti self.smer na miru) in
      # nic drugega. seveda pogleda v self.telo, self.smer in self.igra.polje,
      # da ugotovi, kako nastaviti self.smer.
      pass


##############################################################################################################################

class HumanSnake(Snake):
    def __init__(self, igra, glava_x, glava_y, smer, dolzina):
        super().__init__(igra, glava_x, glava_y, smer, dolzina, "green", "deep pink")

class ComputerSnake(Snake):
    def __init__(self, igra, glava_x, glava_y, smer, dolzina):
        super().__init__(igra, glava_x, glava_y, smer, dolzina, "turquoise", "yellow")

##############################################################################################################################

class SnakeGame():
    def __init__(self,master):
        #Meni (v nastajanju)
        menu = Menu(master)
        master.config(menu=menu) #Dodamo meni.

        game_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Game", menu=game_menu)
        game_menu.add_command(label="New Game        (R)", command=self.new_game)
        game_menu.add_command(label="Quit Game      (Esc)", command=self.quit)
        game_menu.add_separator()
        game_menu.add_command(label="Help", command=self.zacasna)

        player_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Player", menu=player_menu)
        player_menu.add_command(label="Me", command=self.me)
        player_menu.add_command(label="Me vs. Computer", command=self.me_vs_computer)
        player_menu.add_command(label="Me vs. Computers", command=self.me_vs_computers)


        difficulty_menu = Menu(menu, tearoff=0)  #???izberi glede na player_menu
        menu.add_cascade(label="Difficulty", menu=difficulty_menu)
        difficulty_menu.add_command(label="Easy", command=self.zacasna)
        difficulty_menu.add_command(label="Medium", command=self.zacasna)
        difficulty_menu.add_command(label="Hard", command=self.zacasna)


        #Ustvarimo platno
        self.platno = Canvas(master, width = sirina * 10, height = visina * 10, bg = "black") #ustvarimo platno
        self.platno.pack()

        #Upravljanje tipk
        self.platno.bind_all("<Left>",self.levo) #pritisnemo levo za spremembo smeri kace v levo (podobno za desno,gor in dol)
        self.platno.bind_all("<Right>",self.desno)
        self.platno.bind_all("<Up>",self.gor)
        self.platno.bind_all("<Down>",self.dol)
        self.platno.bind_all("<Escape>",self.zapri_okno) #pritisnemo Escape za konec (zapre okno)
        self.platno.bind_all("<s>",self.zacni_igro)  #pritisnemo s za zacetek
        self.platno.bind_all("<r>",self.nova_igra)

        #Opis
        self.platno.create_text(255, 50,fill = "deep pink",font=("Purisa",25),justify=CENTER,text = "SNAKE GAME")
        self.platno.create_text(255, 250,fill = "spring green",justify=CENTER,text = "In this game you will control the snake to eat food. \n\n"
                                                                     "When the snake eats a food, then it will grow and become longer.\n\n"
                                                                     "Eating food will also increase the speed.\n\n "
                                                                     "You should control the snake so that it will not run into walls.\n\n"
                                                                     "it must also not hit itself, otherwise you will lose.\n\n"
                                                                     "You can play alone or against the computer.\n\n"
                                                                     "If you will play against the computer, you be careful not to hit other snakes.\n\n"
                                                                     "Press R to start the game and HAVE FUN! :)")

    ##############################################################################################################################

    def new_game(self):
        """Nastavi vse za začetek igre."""
        # Pobrišemo platno
        self.platno.delete(ALL)
        # Nastavimo polje na prazno
        self.polje = [ [ 0 for i in range(0,sirina) ] for j in range(0,visina) ]
        # naredimo clovekovo kaco
        x_clovek = randint(dolzina+1,sirina-dolzina-1)  #x koordinata glave
        y_clovek = randint(dolzina+1,visina-dolzina-1)  #y koordinata glave
        smer = randint(0,3)
        self.igralec = HumanSnake(self, x_clovek, y_clovek, smer, dolzina)

        # naredimo se racunalnikove kace
        self.kace = []
        for k in range(self.stevilo_racunalnikovih_kac):
            smer_rac = randint(0,3)
            zac_razdalja_kac = 0
            while zac_razdalja_kac <= 20: #Dokler je razdalja med glavama kac <= 20, izberi nov (x,y).
                x_rac = randint(dolzina,sirina-dolzina)
                y_rac = randint(dolzina,visina-dolzina)
                zac_razdalja_kac = abs(x_clovek - x_rac) + abs(y_clovek - y_rac)
            self.kace.append(ComputerSnake(self, x_rac, y_rac, smer_rac, dolzina))

        self.hrana = None
        self.konec = False #to potrebujemo, da vemo kdaj koncati igro
        self.hitrost = 100 #zacetna hitrost
        self.score = 0 #zacetno stevilo pobrane hrane
        self.zmaga_igralec = None

        self.text_score = self.platno.create_text(460, 20,fill = "green",text = "SCORE:" + str(self.score)) #dodamo text (score:0)
        self.text_id = self.platno.create_text(90, 40,fill = "white",text ="  PRESS S TO START")


    ##############################################################################################################################
    def glavni_korak(self):
        if self.konec:
            # igre je konec
            if self.zmaga_igralec:
                # zmagal je igralec
                self.platno.create_text(200, 200,fill = "white",text = "You win!")
            else:
                # igralec je zgubil
                self.platno.create_text(200, 200,fill = "white",text = "You lose!")
            self.platno.create_text(90, 40,fill = "white",text ="Press R to restart\n\nPress Esc to quit")
        else:
            # igra poteka
            # premakni igralca
            if self.igralec.premakni():
              # zaletel se je
              self.konec = True
              self.zmaga_igralec = False

            # premakni racunalnikove kace
            for k in self.kace:
              if self.konec:
                # nekdo se je zaletel, takoj nehamo
                break
              else:
                # izracunaj, v katero smer se obrne kaca
                k.obrni() # tu se kaca obrne, ce AI meni, da se mora obrniti
                if k.premakni():
                    # kaca k se je zaletela
                    self.konec = True
                    self.zmaga_igralec = True

            # ali je treba dodati novo hrano?
            if self.hrana is None:
              self.postavi_hrano()

            # poskrbi, da se spet pokličemo
            self.klici_premakni = self.platno.after(self.hitrost,self.glavni_korak)


    ##############################################################################################################################
    def algoritem_rac(self):

        ###Racunalnik (koordinate)
        rep_rac = self.kace[1].telo[(len(self.kace[1].telo))-1] #iz seznama kaca vzamemo koordinate repa
        self.x_rep_rac = rep_rac[0] #x koordinata repa
        self.y_rep_rac = rep_rac[1]
        glava_rac = self.kace[1].telo[0]
        self.x_glava_rac = glava_rac[0]
        self.y_glava_rac = glava_rac[1]
        mozne_smeri = [self.kace[1].smer,(self.kace[1].smer + 1)%4,(self.kace[1].smer - 1)%4] #seznam moznih smeri
        stevec_nepraznih_polj = 0 #Steje neprazna polja.

        trojice = []
        for i in range(len(mozne_smeri)):
            self.nov_x_rac = self.x_glava_rac + dx[mozne_smeri[i]] #Izracunamo nov x in nov y za vse mozne smeri.
            self.nov_y_rac = self.y_glava_rac + dy[mozne_smeri[i]]
            if self.nov_x_rac == 50 or self.nov_x_rac == -1 or self.nov_y_rac == 50 or self.nov_y_rac == -1:
                stevec_nepraznih_polj += 1
                continue

            if self.polje[self.nov_y_rac][self.nov_x_rac] == 5:  #Ce je na polju hrana, funkcija vrne nov (x,y) polja, kjer lezi hrana.
                self.kace[1].smer = mozne_smeri[i]
                return(self.nov_x_rac,self.nov_y_rac,self.kace[1].smer)

            elif self.polje[self.nov_y_rac][self.nov_x_rac] == 0:
                razdalja_hrana = abs(self.nov_x_rac - self.x_hrana) + abs(self.nov_y_rac - self.y_hrana)
                self.kace[1].smer = mozne_smeri[i]
                trojice.append((self.nov_x_rac,self.nov_y_rac,razdalja_hrana,self.kace[1].smer))
            else:
                stevec_nepraznih_polj += 1

        if stevec_nepraznih_polj == 3:  #Ce ni nobenega praznega polja, koncaj igro.
            self.konec = True   #konec igre-racunalnik se zaleti (zgubi?)
            self.zmaga_igralec = True
        else:
            random.shuffle(trojice) #Premesamo seznam, da je koordinata (x,y) z najmanjso razdaljo izbrana nakljucno.

            trojica_min = min(trojice, key = lambda t: t[2])

            self.nov_x_rac = trojica_min[0]
            self.nov_y_rac = trojica_min[1]
            self.kace[1].smer = trojica_min[3]
            return(self.nov_x_rac,self.nov_y_rac,self.kace[1].smer)

    ##############################################################################################################################
    def hrana(self):
        #Ce se ni hrane, jo postavi.
        if not self.polje_hrana:
            self.postavi_hrano()

        #Ce je glava igralca na hrani, jo poje.
        if self.nov_x_glava == self.x_hrana and self.nov_y_glava == self.y_hrana:
            self.poje_hrano()

    ##############################################################################################################################

    def postavi_hrano(self):
        x_hrana = randint(1,sirina-2) #Postavimo hrano na "random" mesto (ne more biti na robu).
        y_hrana = randint(1,visina-2)

        if self.polje[y_hrana][x_hrana] == 0:
            # Polje je prazno, postavi hrano
            self.polje[y_hrana][x_hrana] = 2
            id_hrana = self.platno.create_rectangle(x_hrana*10, y_hrana*10, (x_hrana+1)*10,(y_hrana+1)*10,width=1,outline="pink")
            self.hrana = (x_hrana, y_hrana, id_hrana)


    def odstrani_hrano(self):
        if self.hrana is not None:
            self.platno.delete(self.hrana[2])
            self.hrana = None

    def poje_hrano_rac(self):
        self.id_dodan_rep_rac = self.platno.create_oval(self.x_rep_rac*10, self.y_rep_rac*10,(self.x_rep_rac+1)*10,(self.y_rep_rac+1)*10,width=5,outline="turquoise") #narišemo podaljšani del kače
        self.polje[self.y_rep_rac][self.x_rep_rac] = 1
        self.kace[1].telo.append((self.x_rep_rac,self.y_rep_rac,self.id_dodan_rep_rac))
        self.platno.delete(self.id_hrana)
        self.polje_hrana = False

        self.platno.delete(self.text_score_rac) #zbrisemo prejsnji "score"
        self.score_rac += 1 #stevec za "score"
        self.text_score_rac = self.platno.create_text(430, 40,fill = "turquoise",text = "SCORE COMPUTER:" + str(self.score_rac)) #ponovno izpisemo "score"


    ###################FUNKCIJE ZA KLIC TIPK##################
    def desno(self,event):   #Ce pritisnemo desno, se smer kace obrne v desno.
        if self.igralec.smer == 0  or self.igralec.smer == 2:
            self.igralec.smer = 1

    def levo(self,event):
        if self.igralec.smer == 0  or self.igralec.smer == 2:
            self.igralec.smer = 3

    def dol(self,event):
        if self.igralec.smer == 1  or self.igralec.smer == 3:
            self.igralec.smer = 2

    def gor(self,event):
        if self.igralec.smer == 1  or self.igralec.smer == 3:
            self.igralec.smer = 0

    def zapri_okno(self, event): #Funkcija, ki jo klicemo, ko pritisnemo tipko Escape.
        self.platno.master.destroy()

    def zacni_igro(self,event): #Funkcija, ki jo klicemo, ko pritisnemo tipko s.
        self.platno.delete(self.text_id) #izbrisemo text (Start)
        self.glavni_korak()

    def nova_igra(self,event): #Funkcija, ki jo klicemo, ko pritisnemo tipko r.
        self.platno.delete(ALL)
        self.new_game()

    ###funkciji za Menu (funkcija s parametri (self,event) ne deluje)
    def quit(self):
        self.platno.master.destroy()

    ###Pomozne funkcije###
    def zacasna(self):
        pass

    #na zacetku je rac_ena in rac_dve False
    def me(self):
        self.stevilo_racunalnikovih_kac = 0
        self.new_game()

    def me_vs_computer(self):
        self.stevilo_racunalnikovih_kac = 1
        self.new_game()

    def me_vs_computers(self):
        self.stevilo_racunalnikovih_kac = 3
        self.new_game()



root = Tk()   #nardi osnovno, prazno,okno, še ne pokaže - naložiš gumbe,platno,....
root.title("Snake Game")  #ime
aplikacija = SnakeGame(root)  #to naredi objekt pod imenom aplikacija
root.mainloop()