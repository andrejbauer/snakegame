
from tkinter import *
from random import *
import random

visina = 50
sirina = 50
dolzina = 10
dx = [0,1,0,-1]
dy = [-1,0,1,0]

obratna = [2, 3, 0,1]

class Snake():
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina, barva, barva_glave):
        self.canvas = canvas # Canvas, na katerem je kača narisana
        self.telo = []
        for i in range(dolzina):
            x = glava_x + dx[obratna[smer]] * i
            y = glava_y + dy[obratna[smer]] * i
            barva_ovala = (barva_glave if i == 0 else barva)
            id = canvas.create_oval(x*10,y*10,(x+1)*10,(y+1)*10,width=5,outline=barva_ovala)
            self.telo.append((x,y,id))
        self.smer = smer
        self.barva = barva
        self.barva_glave = barva_glave

    def premakni(self):
        ###Igralec (koordinate)
        (x_rep, y_rep) = self.telo[-1]
        (x_glava, y_glava) = self.telo[0]
        nov_x_glava = self.x_glava + dx[self.smer] #spremenimo x v tisto smer, v katero se premika kaca
        nov_y_glava = self.y_glava + dy[self.smer] #isto za y


class HumanSnake(Snake):
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina):
        super().__init__(canvas, glava_x, glava_y, smer, dolzina, "green", "deep pink")

class ComputerSnake(Snake):
    def __init__(self, canvas, glava_x, glava_y, smer, dolzina):
        super().__init__(canvas, glava_x, glava_y, smer, dolzina, "turquoise", "yellow")


class SnakeGame():
    def __init__(self,master):
        #Meni (v nastajanju)
        menu = Menu(master)
        master.config(menu=menu) #Dodamo meni.

        game_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Game", menu=game_menu)
        game_menu.add_command(label="New Game        (R)", command=self.new_game)
        game_menu.add_command(label="Quit Game      (Esc)", command=self.quit)
        game_menu.add_separator()
        game_menu.add_command(label="Help", command=self.zacasna)

        player_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Player", menu=player_menu)
        player_menu.add_command(label="Me", command=self.me)
        player_menu.add_command(label="Me vs. Computer", command=self.me_vs_computer)
        player_menu.add_command(label="Me vs. Computers", command=self.me_vs_computers)


        difficulty_menu = Menu(menu, tearoff=0)  #???izberi glede na player_menu
        menu.add_cascade(label="Difficulty", menu=difficulty_menu)
        difficulty_menu.add_command(label="Easy", command=self.zacasna)
        difficulty_menu.add_command(label="Medium", command=self.zacasna)
        difficulty_menu.add_command(label="Hard", command=self.zacasna)


        #Ustvarimo platno
        self.platno = Canvas(master, width = sirina * 10, height = visina * 10, bg = "black") #ustvarimo platno
        self.platno.pack()

        #Upravljanje tipk
        self.platno.bind_all("<Left>",self.levo) #pritisnemo levo za spremembo smeri kace v levo (podobno za desno,gor in dol)
        self.platno.bind_all("<Right>",self.desno)
        self.platno.bind_all("<Up>",self.gor)
        self.platno.bind_all("<Down>",self.dol)
        self.platno.bind_all("<Escape>",self.zapri_okno) #pritisnemo Escape za konec (zapre okno)
        self.platno.bind_all("<s>",self.zacni_igro)  #pritisnemo s za zacetek
        self.platno.bind_all("<r>",self.nova_igra)

        #Opis
        self.platno.create_text(255, 50,fill = "deep pink",font=("Purisa",25),justify=CENTER,text = "SNAKE GAME")
        self.platno.create_text(255, 250,fill = "spring green",justify=CENTER,text = "In this game you will control the snake to eat food. \n\n"
                                                                     "When the snake eats a food, then it will grow and become longer.\n\n"
                                                                     "Eating food will also increase the speed.\n\n "
                                                                     "You should control the snake so that it will not run into walls.\n\n"
                                                                     "it must also not hit itself, otherwise you will lose.\n\n"
                                                                     "You can play alone or against the computer.\n\n"
                                                                     "If you will play against the computer, you be careful not to hit other snakes.\n\n"
                                                                     "Press R to start the game and HAVE FUN! :)")

    ##############################################################################################################################

    def new_game(self):
        """Nastavi vse za začetek igre."""
        # Pobrišemo platno
        self.platno.delete(ALL)
        # Nastavimo polje
        self.polje = [ [ 0 for i in range(0,sirina) ] for j in range(0,visina) ]
        # naredimo clovekovo kaco
        x_clovek = randint(dolzina+1,sirina-dolzina-1)  #x koordinata glave
        y_clovek = randint(dolzina+1,visina-dolzina-1)  #y koordinata glave
        smer = randint(0,3)
        self.igralec = HumanSnake(self.platno, x_clovek, y_clovek, smer, dolzina)
         #narisemo kaco v polje
        for i in range(len(self.kace[0].telo)):
            (x_telo, y_telo) = self_igralec.telo[i]
            self.polje[y_telo][x_telo] = 1

        # naredimo se racunalnikove kace
        self.racunalnikove_kace = []
        for k in range(self.stevilo_racunalnikovih_kac):
            smer_rac = randint(0,3)
            zac_razdalja_kac = 0
            while zac_razdalja_kac <= 20: #Dokler je razdalja med glavama kac <= 20, izberi nov (x,y).
                x_rac = randint(dolzina,sirina-dolzina)
                y_rac = randint(dolzina,visina-dolzina)
                zac_razdalja_kac = abs(x_clovek - x_rac) + abs(y_clovek - y_rac)
            self.racunalnikove_kace.append(ComputerSnake(self.platno, x_rac, y_rac, smer_rac, dolzina))
            for i in range(len(self.kace[k].telo)):
                trojica = self.kace[k].telo[i]
                x_telo = trojica[0]
                y_telo = trojica[1]
                self.polje[y_telo][x_telo] = 1

        self.id_hrana = 0 #tu bomo shranjevali "id-je" narisane hrane
        self.x_hrana = 0 #x koordinata hrane
        self.y_hrana = 0 #y koordinata hrane
        self.polje_hrana = False #to potrebujemo, da se hrana ne pojavi dokler ne zgine prejsnja
        self.konec = False #to potrebujemo, da vemo kdaj koncati igro
        self.hitrost = 100 #zacetna hitrost
        self.score = 0 #zacetno stevilo pobrane hrane
        self.zmaga_igralec = None

        self.text_score = self.platno.create_text(460, 20,fill = "green",text = "SCORE:" + str(self.score)) #dodamo text (score:0)
        self.text_id = self.platno.create_text(90, 40,fill = "white",text ="  PRESS S TO START")

        if self.rac_ena or self.rac_dve:
            self.score_rac = 0 #zacetno stevilo pobrane hrane
            self.text_score_rac = self.platno.create_text(430, 40,fill = "turquoise",text = "SCORE COMPUTER:" + str(self.score_rac)) #dodamo text (score:0)

        if self.rac_dve:
            self.score_rac2 = 0 #zacetno stevilo pobrane hrane
            self.text_score_rac2 = self.platno.create_text(430, 60,fill = "orange red",text = "SCORE COMPUTER 2:" + str(self.score_rac2)) #dodamo text (score:0)

    ##############################################################################################################################

    def premakni(self):
        self.platno.delete(self.text_id) #izbrisemo text (Start)

        ###Igralec (koordinate)
        rep = self.kace[0].telo[dolzina-1]
        self.x_rep = rep[0] #x koordinata repa
        self.y_rep = rep[1]
        glava = self.kace[0].telo[0]
        self.x_glava = glava[0] #x koordinata glave
        self.y_glava = glava[1] #y koordinata glave
        self.nov_x_glava = self.x_glava + dx[self.kace[0].smer] #spremenimo x v tisto smer, v katero se premika kaca
        self.nov_y_glava = self.y_glava + dy[self.kace[0].smer] #isto za y


        if not self.konec: #ce se ni konec igre, preverimo, ce se zaleti, postavimo hrano itd.
            self.preverjanje_polj()

        if not self.konec:  #ce se ni konec, narisemo premik
            self.narisi_premik(0,self.x_glava,self.y_glava,self.nov_x_glava,self.nov_y_glava,self.x_rep,self.y_rep,"green","deep pink")
            if self.rac_ena or self.rac_dve and not self.konec: #ce je v igri se racunalnik
                self.algoritem_rac()
                self.preverjanje_polj_rac()
                self.narisi_premik(1,self.x_glava_rac,self.y_glava_rac,self.nov_x_rac,self.nov_y_rac,self.x_rep_rac,self.y_rep_rac,"turquoise","yellow")


            self.klici_premakni = self.platno.after(self.hitrost,self.premakni)

        else:  #Ce je konec (npr. ce smo se zaleteli) se izpise napis Game over ipd.
            if self.zmaga_igralec:
                self.platno.create_text(200, 200,fill = "white",text = "You win!")
                self.text2_id = self.platno.create_text(90, 40,fill = "white",text ="Press R to Restart \n\n Press Esc To quit")
            elif not self.zmaga_igralec:
                self.platno.create_text(200, 200,fill = "white",text = "You lose! Press R and Try again!")

    ##############################################################################################################################
    def algoritem_rac(self):

        ###Racunalnik (koordinate)
        rep_rac = self.kace[1].telo[(len(self.kace[1].telo))-1] #iz seznama kaca vzamemo koordinate repa
        self.x_rep_rac = rep_rac[0] #x koordinata repa
        self.y_rep_rac = rep_rac[1]
        glava_rac = self.kace[1].telo[0]
        self.x_glava_rac = glava_rac[0]
        self.y_glava_rac = glava_rac[1]
        mozne_smeri = [self.kace[1].smer,(self.kace[1].smer + 1)%4,(self.kace[1].smer - 1)%4] #seznam moznih smeri
        stevec_nepraznih_polj = 0 #Steje neprazna polja.

        trojice = []
        for i in range(len(mozne_smeri)):
            self.nov_x_rac = self.x_glava_rac + dx[mozne_smeri[i]] #Izracunamo nov x in nov y za vse mozne smeri.
            self.nov_y_rac = self.y_glava_rac + dy[mozne_smeri[i]]
            if self.nov_x_rac == 50 or self.nov_x_rac == -1 or self.nov_y_rac == 50 or self.nov_y_rac == -1:
                stevec_nepraznih_polj += 1
                continue

            if self.polje[self.nov_y_rac][self.nov_x_rac] == 5:  #Ce je na polju hrana, funkcija vrne nov (x,y) polja, kjer lezi hrana.
                self.kace[1].smer = mozne_smeri[i]
                return(self.nov_x_rac,self.nov_y_rac,self.kace[1].smer)

            elif self.polje[self.nov_y_rac][self.nov_x_rac] == 0:
                razdalja_hrana = abs(self.nov_x_rac - self.x_hrana) + abs(self.nov_y_rac - self.y_hrana)
                self.kace[1].smer = mozne_smeri[i]
                trojice.append((self.nov_x_rac,self.nov_y_rac,razdalja_hrana,self.kace[1].smer))
            else:
                stevec_nepraznih_polj += 1

        if stevec_nepraznih_polj == 3:  #Ce ni nobenega praznega polja, koncaj igro.
            self.konec = True   #konec igre-racunalnik se zaleti (zgubi?)
            self.zmaga_igralec = True
        else:
            random.shuffle(trojice) #Premesamo seznam, da je koordinata (x,y) z najmanjso razdaljo izbrana nakljucno.

            trojica_min = min(trojice, key = lambda t: t[2])

            self.nov_x_rac = trojica_min[0]
            self.nov_y_rac = trojica_min[1]
            self.kace[1].smer = trojica_min[3]
            return(self.nov_x_rac,self.nov_y_rac,self.kace[1].smer)

    ##############################################################################################################################

    def preverjanje_polj(self):
        #Ce se ni hrane, jo postavi.
        if not self.polje_hrana:
            self.hrana()

        #Ce je glava igralca na hrani, jo poje.
        if self.nov_x_glava == self.x_hrana and self.nov_y_glava == self.y_hrana:
            self.poje_hrano()

        #Ce se igralec zaleti v rob, je konec igre
        if self.nov_x_glava == -1 or self.nov_x_glava == 50 or self.nov_y_glava == -1 or self.nov_y_glava == 50:
            self.zmaga_igralec = False
            self.konec = True

        #Ce se zaleti v karkoli, je konec igre.
        if not self.konec:
            if self.polje[self.nov_y_glava][self.nov_x_glava] == 1:
                self.konec = True
                self.zmaga_igralec = False


    ##############################################################################################################################

    def preverjanje_polj_rac(self):
        #Ce je glava racunalnika na hrani, jo poje.
        if self.nov_x_rac == self.x_hrana and self.nov_y_rac == self.y_hrana:
            self.poje_hrano_rac()

        #Ce se racunalnik zaleti v rob, je konec igre
        if self.nov_x_rac == -1 or self.nov_x_rac == 50 or self.nov_y_rac == -1 or self.nov_y_rac == 50:
            self.konec = True
            self.zmaga_igralec = True

        #Ce se zaleti v drugo kaco, je konec igre.
        if self.polje[self.nov_y_rac][self.nov_x_rac] == 1:
            self.konec = True
            self.zmaga_igralec = True

    ##############################################################################################################################

    def hrana(self):
        self.x_hrana = randint(1,sirina-2) #Postavimo hrano na "random" mesto (ne more biti na robu).
        self.y_hrana = randint(1,visina-2)

        if self.polje[self.y_hrana][self.x_hrana] == 0: #Hrana se ustvari samo, Ce je polje prazno.
            self.polje[self.y_hrana][self.x_hrana] = 5  #Vrednost polja v matriki, kjer je hrana je 5.
            self.id_hrana = self.platno.create_rectangle(self.x_hrana*10,self.y_hrana*10,(self.x_hrana+1)*10,(self.y_hrana+1)*10,width=1,outline="pink")
            self.polje_hrana = True


    def poje_hrano(self):
        self.id_dodan_rep = self.platno.create_oval(self.x_rep*10, self.y_rep*10,(self.x_rep+1)*10,(self.y_rep+1)*10,width=5,outline="green") #narišemo podaljšani del kače
        self.polje[self.y_rep][self.x_rep] = 1
        self.kace[0].telo.append((self.x_rep,self.y_rep,self.id_dodan_rep))
        self.platno.delete(self.id_hrana)
        self.polje_hrana = False

        self.platno.delete(self.text_score) #zbrisemo prejsnji "score"
        self.score += 1 #stevec za "score"
        self.text_score = self.platno.create_text(460, 20,fill = "green",text = "SCORE:" + str(self.score)) #ponovno izpisemo "score"

        if not self.rac_ena and not self.rac_dve:
            if len(self.kace[0].telo) == 200:
                self.konec = True
                self.zmaga_igralec = True

        if not self.hitrost == 10: #Ko kaca pobere hrano se hitrost poveca za 1ms.
            self.hitrost = self.hitrost - 1


    def poje_hrano_rac(self):
        self.id_dodan_rep_rac = self.platno.create_oval(self.x_rep_rac*10, self.y_rep_rac*10,(self.x_rep_rac+1)*10,(self.y_rep_rac+1)*10,width=5,outline="turquoise") #narišemo podaljšani del kače
        self.polje[self.y_rep_rac][self.x_rep_rac] = 1
        self.kace[1].telo.append((self.x_rep_rac,self.y_rep_rac,self.id_dodan_rep_rac))
        self.platno.delete(self.id_hrana)
        self.polje_hrana = False

        self.platno.delete(self.text_score_rac) #zbrisemo prejsnji "score"
        self.score_rac += 1 #stevec za "score"
        self.text_score_rac = self.platno.create_text(430, 40,fill = "turquoise",text = "SCORE COMPUTER:" + str(self.score_rac)) #ponovno izpisemo "score"



    ##############################################################################################################################

    def narisi_premik(self,st_kace,x_glava,y_glava,nov_x_glava,nov_y_glava,x_rep,y_rep,barva,barva_glave):
            #RISANJE:(1) zbrisemo rep (2) zbrisemo staro glavo, (3) narisemo staro glavo z drugo barvo (telo). (4) narisemo novo glavo

            #(1)
            self.trojica_rep = self.kace[st_kace].telo[len(self.kace[st_kace].telo)-1]
            self.platno.delete(self.trojica_rep[2])

            #(2)
            self.trojica_stara_glava = self.kace[st_kace].telo[0]
            self.platno.delete(self.trojica_stara_glava[2])

            #(3)
            self.id_stara_glava = self.platno.create_oval(x_glava*10,y_glava*10,(x_glava+1)*10,(y_glava+1)*10,width=5,outline=barva)

            self.kace[st_kace].telo.pop(0)  #v trojici stare glave moramo zamenjati ID: izbrisemo celo trojico stare glave
            self.kace[st_kace].telo.insert(0,(x_glava,y_glava,self.id_stara_glava)) #in dodamo novo

            #SPREMEMBE V SEZNAMU KACA
            self.kace[st_kace].telo.insert(0,(nov_x_glava,nov_y_glava)) #na zacetek seznama dodamo koordinate premaknjene glave
            self.kace[st_kace].telo.pop(len(self.kace[st_kace].telo)-1) #izbrisemo rep

            #SPREMEMBE V MATRIKI
            self.polje[y_glava][x_glava] = 1 #v matriko vrednosti dodamo 1, kjer je bla prej glava
            self.polje[self.trojica_rep[1]][self.trojica_rep[0]] = 0 #dodamo 0, kjer je bil prej rep
            self.polje[nov_y_glava][nov_x_glava] = 1 #dodamo 1, kjer je nova glava

            #(4)
            self.id_kaca_glava = self.platno.create_oval(nov_x_glava*10,nov_y_glava*10,(nov_x_glava+1)*10,(nov_y_glava+1)*10,width=5,outline=barva_glave) #narisemo novo glavo in shranimo id nove glave
            self.kace[st_kace].telo[0] += (self.id_kaca_glava,) #id nove glave dodamo v zacetni element kace, na zadnje mesto


    ###################FUNKCIJE ZA KLIC TIPK##################
    def desno(self,event):   #Ce pritisnemo desno, se smer kace obrne v desno.
        if self.kace[0].smer == 0  or self.kace[0].smer == 2:
            self.kace[0].smer = 1

    def levo(self,event):
        if self.kace[0].smer == 0  or self.kace[0].smer == 2:
            self.kace[0].smer = 3

    def dol(self,event):
        if self.kace[0].smer == 1  or self.kace[0].smer == 3:
            self.kace[0].smer = 2

    def gor(self,event):
        if self.kace[0].smer == 1  or self.kace[0].smer == 3:
            self.kace[0].smer = 0

    def zapri_okno(self, event): #Funkcija, ki jo klicemo, ko pritisnemo tipko Escape.
        self.platno.master.destroy()

    def zacni_igro(self,event): #Funkcija, ki jo klicemo, ko pritisnemo tipko s.
        self.premakni()

    def nova_igra(self,event):
        self.platno.delete(ALL)
        self.new_game()

    ###funkciji za Menu (funkcija s parametri (self,event) ne deluje)
    def quit(self):
        self.platno.master.destroy()

    ###Pomozne funkcije###
    def zacasna(self):
        pass
    #na zacetku je rac_ena in rac_dve False
    def me(self):
        self.stevilo_racunalnikovih_kac = 0
        self.new_game()

    def me_vs_computer(self):
        self.stevilo_racunalnikovih_kac = 1
        self.new_game()

    def me_vs_computers(self):
        self.stevilo_racunalnikovih_kac = 3
        self.new_game()



root = Tk()   #nardi osnovno, prazno,okno, še ne pokaže - naložiš gumbe,platno,....
root.title("Snake Game")  #ime
aplikacija = SnakeGame(root)  #to naredi objekt pod imenom aplikacija
root.mainloop()
